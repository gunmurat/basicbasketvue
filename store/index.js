import shoppingService from '@/server/api/shopping_service.js';

const state = () => ({
    products: [],
    basket: null,
});

const getters = {
    getProducts: state => state.products,
    getProductById: state => id => state.products.find(product => product._id === id),
    getBasket: state => state.basket,
};

const mutations = {
    SET_PRODUCTS: (state, products) => state.products = products,
    ADD_NEW_PRODUCT: (state, product) => state.products.push(product),
    ADD_PRODUCT_TO_BASKET: (state, product) => state.basket.items.push(product),
    REMOVE_PRODUCT_FROM_BASKET: (state, product) => state.basket.products = state.basket.products.filter(p => p._id !== product._id),
    SET_BASKET: (state, basket) => state.basket = basket,
}

const actions = {
    async fetchAllProducts({ commit }) {
        const products = await shoppingService.getAllProducts();
        commit('SET_PRODUCTS', products.data);
    },
    async fetchProductById({ commit }, id) {
        const product = await shoppingService.getProductById(id);
        commit('ADD_NEW_PRODUCT', product);
    },
    async createProduct({ commit }, product) {
        const newProduct = await shoppingService.createProduct(product);
        commit('ADD_NEW_PRODUCT', newProduct);
    },
    async createBasket({ commit }) {
        const newBasket = await shoppingService.createBasket();
        commit('SET_BASKET', newBasket.data);
    },
    async fetchBasketById({ commit }, id) {
        const basket = await shoppingService.getBasketById(id);
        commit('SET_BASKET', basket.data);
    },
    async addProductToBasket({ commit }, data) {
        const addedProduct = await shoppingService.addProductToBasket(data.basketId, data.productId, data.quantity);
        commit('ADD_PRODUCT_TO_BASKET', { productID: addedProduct.productID, quantity: addedProduct.quantity || 1 });
    },
    async removeProductFromBasket({ commit }, productId) {
        const removedProduct = await shoppingService.removeProductFromBasket(this.state.basket._id, productId);
        commit('REMOVE_PRODUCT_FROM_BASKET', removedProduct.data);
    },
    async fetchAllBaskets({ commit }) {
        const baskets = await shoppingService.getAllBaskets();
        if (Array.isArray(baskets.data)) {
            commit('SET_BASKET', baskets.data[0]);
        } else {
            commit('SET_BASKET', null);
        }
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}


