import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)

// import components
import ProductCard from '@/components/ProductCard.vue'
import ProductList from '@/components/ProductList.vue'
import CartItem from '@/components/CartItem.vue'

// import pages
import IndexPage from '@/pages/index.vue'
import CartPage from '@/pages/cart/index.vue'

const mockProducts = [
    {
        _id: "6230e6808ed47e30845fa210",
        name: 'Product 1',
        description: 'Description 1',
        price: 10,
        image: 'https://via.placeholder.com/150',
        stock: 10,
    },
    {
        _id: "6230e6808ed47e30845fa211",
        name: 'Product 2',
        description: 'Description 2',
        price: 20,
        image: 'https://via.placeholder.com/150',
        stock: 20,
    },
];

const mockCartItems = [
    {
        product: mockProducts[0],
        quantity: 1,
        price: mockProducts[0].price * 1,
    },
    {
        product: mockProducts[1],
        quantity: 2,
        price: mockProducts[1].price * 2,
    },
];

describe('Product Card Component Test', () => {
    let store
    let actions

    beforeEach(() => {
        actions = {
            fetchProductById: jest.fn(),
            fetchAllProducts: jest.fn(),
            addProductToBasket: jest.fn(),
        }
        store = new Vuex.Store({
            actions,
            getters: {
                getProductById: state => mockProducts[0],
                getProducts: state => mockProducts,
                getBasket: state => state.basket,
            },
            state: {
                products: mockProducts,
                basket: {
                    items: [],
                    _id: "6230e6808ed47e30845fa210",
                    totalAmount: 0,
                },

            },
        })
    })

    test('should ProductCard visible', () => {
        const wrapper = shallowMount(ProductCard, {
            store,
            localVue,
            propsData: {
                product: mockProducts[0],
            }
        })

        expect(wrapper.isVisible()).toBe(true)
        expect(wrapper.find('#product-name').text()).toBe(mockProducts[0].name)
    })

    test('should add the cart', async () => {
        const wrapper = shallowMount(ProductCard, {
            store,
            localVue,
            propsData: {
                product: mockProducts[0],
            }
        })

        await wrapper.find('#add-to-cart').trigger('click')
        expect(actions.addProductToBasket).toHaveBeenCalled()
    })

})

describe('Cart Item Component Test', () => {
    let store
    let actions

    beforeEach(() => {
        actions = {
            fetchProductById: jest.fn(),
            fetchAllProducts: jest.fn(),
            addProductToBasket: jest.fn(),
            removeProductFromBasket: jest.fn(),
        }
        store = new Vuex.Store({
            actions,
            getters: {
                getProductById: state => mockProducts[0],
                getProducts: state => mockProducts,
                getBasket: state => state.basket,
            },
            state: {
                products: mockProducts,
                basket: {
                    items: mockCartItems,
                    _id: "6230e6808ed47e30845fa210",
                    totalAmount: 30,
                },

            },
        })
    })

    test('should CartItem visible', () => {
        const wrapper = shallowMount(CartItem, {
            store,
            localVue,
            propsData: {
                cartItem: mockCartItems[0],
                basketID: "6230e6808ed47e30845fa210",
            }
        });

        expect(wrapper.isVisible()).toBe(true)
    })

    test('should CartItem remove ', async () => {
        const wrapper = shallowMount(CartItem, {
            store,
            localVue,
            propsData: {
                cartItem: mockCartItems[0],
                basketID: "6230e6808ed47e30845fa210",
            }
        });

        await wrapper.find('#remove-from-cart').trigger('click')
        expect(actions.removeProductFromBasket).toHaveBeenCalled()
    })

    test('should CartItem change quantity', async () => {
        const wrapper = shallowMount(CartItem, {
            store,
            localVue,
            propsData: {
                cartItem: mockCartItems[0],
                basketID: "6230e6808ed47e30845fa210",
            }
        });

        await wrapper.find('#item-quantity').trigger('input')
        expect(actions.addProductToBasket).toHaveBeenCalled()
    })
});

describe('Product List Component Test', () => {
    test('should ProductList visible', () => {
        const wrapper = shallowMount(ProductList, {
            localVue,
            propsData: {
                products: mockProducts,
            }
        })

        expect(wrapper.isVisible()).toBe(true)
        expect(wrapper.findComponent(ProductCard).exists()).toBe(true)
    });
})

describe('Index Page Components Test', () => {
    let store
    let actions

    beforeEach(() => {
        actions = {
            fetchAllProducts: jest.fn(),
            fetchAllBaskets: jest.fn(),
            createBasket: jest.fn(),
        }
        store = new Vuex.Store({
            actions,
            getters: {
                getProducts: state => mockProducts,
                getBasket: state => state.basket,
            },
            state: {
                products: [],
                basket: null,
            },
        })
    })

    test('should ProductList component visible in index page', () => {
        const wrapper = shallowMount(IndexPage, {
            store,
            localVue,
        })

        expect(wrapper.findComponent(ProductList).exists()).toBe(true)
    })

    test('should init functions triggered', async () => {
        const wrapper = shallowMount(IndexPage, {
            store,
            localVue,
        })

        await wrapper.vm.$options.asyncData({ store })
        expect(actions.fetchAllProducts).toHaveBeenCalled()
        expect(actions.fetchAllBaskets).toHaveBeenCalled()
        expect(actions.createBasket).toHaveBeenCalled()

    })

})

describe('Cart Page Components Test', () => {
    let store
    let actions

    beforeEach(() => {
        actions = {
            fetchAllProducts: jest.fn(),
            fetchAllBaskets: jest.fn(),
            createBasket: jest.fn(),
        }
        store = new Vuex.Store({
            actions,
            getters: {
                getProducts: state => mockProducts,
                getBasket: state => state.basket,
            },
            state: {
                products: [],
                basket: null,
            },
        })
    })

    const mockRouter = {
        push: jest.fn()
    }

    test('should asyncData and created functions triggered', async () => {
        const wrapper = shallowMount(CartPage, {
            store,
            localVue,
            mocks: {
                $router: mockRouter,
            }
        })

        await wrapper.vm.$options.asyncData({ store })
        expect(actions.fetchAllBaskets).toHaveBeenCalled()
        expect(mockRouter.push).toHaveBeenCalled()

    })
})







