import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://localhost:5000',
    timeout: 2000,
});

async function getAllProducts() {
    const products = await instance.get('/products');
    if (products.status === 200) {
        return products.data;
    }
    return [];
}

async function getProductById(id) {
    const product = await instance.get(`/products/${id}`);
    if (product.status === 200) {
        return product.data;
    }
    return null;
}

async function createProduct(product) {
    const newProduct = await instance.post('/products', product);
    if (newProduct.status === 200) {
        return newProduct.data;
    }
    return null;
}

async function createBasket() {
    const newBasket = await instance.post('/baskets');
    if (newBasket.status === 200) {
        return newBasket.data;
    }
    return null;
}

async function getBasketById(id) {
    const basket = await instance.get(`/baskets/${id}`);
    if (basket.status === 200) {
        return basket.data;
    }
    return null;
}

async function addProductToBasket(basketId, productId, quantity) {
    const addedProduct = await instance.post(`/baskets/${basketId}`, { productID: productId, quantity: quantity });
    if (addedProduct.status === 200) {
        return addedProduct.data;
    }
    return null;
}

async function removeProductFromBasket(basketId, productId) {
    const removedProduct = await instance.delete(`/baskets/${basketId}/products/${productId}`);
    if (removedProduct.status === 200) {
        return removedProduct.data;
    }
    return null;
}

async function getAllBaskets() {
    const baskets = await instance.get('/baskets');
    if (baskets.status === 200) {
        return baskets.data;
    }
    return null;
}

export default {
    getAllProducts,
    getProductById,
    createProduct,
    createBasket,
    getBasketById,
    addProductToBasket,
    removeProductFromBasket,
    getAllBaskets
}